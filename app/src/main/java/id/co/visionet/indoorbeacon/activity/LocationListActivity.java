package id.co.visionet.indoorbeacon.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.estimote.coresdk.common.requirements.SystemRequirementsChecker;
import com.estimote.indoorsdk_module.cloud.Location;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import id.co.visionet.indoorbeacon.R;
import id.co.visionet.indoorbeacon.adapter.LocationListAdapter;

import static id.co.visionet.indoorbeacon.core.CoreApplication.locationsById;

public class LocationListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private TextView noLocationView;
    private LocationListAdapter locationListAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private List<Location> locations = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_list);

        initView();
    }

    @Override
    protected void onStart() {
        super.onStart();
        SystemRequirementsChecker.checkWithDefaultDialogs(this);
        List<Location> locationIds = new ArrayList<Location>(locationsById.values());
        if (locationIds.isEmpty()) {
            noLocationView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            locations.clear();
            locations.addAll(locationIds);
            locationListAdapter.notifyDataSetChanged();
        }
    }

    private void initView() {
        noLocationView = findViewById(R.id.no_locations_view);

        recyclerView = findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        locationListAdapter = new LocationListAdapter(this, locations);
        recyclerView.setAdapter(locationListAdapter);
        locationListAdapter.setOnItemClickListener(new LocationListAdapter.onItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                Location location = locations.get(position);
                Intent intent = new Intent(LocationListActivity.this, IndoorLocationActivity.class);
                intent.putExtra("locationId", location.getIdentifier());
                startActivity(intent);
            }
        });
    }
}
