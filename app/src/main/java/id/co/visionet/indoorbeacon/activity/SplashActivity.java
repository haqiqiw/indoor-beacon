package id.co.visionet.indoorbeacon.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import com.estimote.indoorsdk_module.cloud.CloudCallback;
import com.estimote.indoorsdk_module.cloud.EstimoteCloudException;
import com.estimote.indoorsdk_module.cloud.IndoorCloudManager;
import com.estimote.indoorsdk_module.cloud.IndoorCloudManagerFactory;
import com.estimote.indoorsdk_module.cloud.Location;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import id.co.visionet.indoorbeacon.R;

import static id.co.visionet.indoorbeacon.core.CoreApplication.cloudCredentials;
import static id.co.visionet.indoorbeacon.core.CoreApplication.locationsById;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Make actionbar invisible.
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().hide();

        setContentView(R.layout.activity_splash);

        IndoorCloudManager cloudManager = new IndoorCloudManagerFactory().create(this, cloudCredentials);
        cloudManager.getAllLocations(new CloudCallback<List<Location>>() {
            @Override
            public void success(List<Location> locations) {
                Map<String, Location> locationIds = new HashMap<>();
                for (Location location: locations) {
                    locationIds.put(location.getIdentifier(), location);
                }
                locationsById.putAll(locationIds);

                startMainActivity();
            }

            @Override
            public void failure(EstimoteCloudException e) {
                Toast.makeText(SplashActivity.this, "Unable to fetch location data from cloud. " +
                        "Check your internet connection and make sure you initialised our SDK with your AppId/AppToken", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void startMainActivity() {
        startActivity(new Intent(this, LocationListActivity.class));
        finish();
    }
}
