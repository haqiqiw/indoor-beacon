package id.co.visionet.indoorbeacon.core;

import android.app.Application;

import com.estimote.cloud_plugin.common.EstimoteCloudCredentials;
import com.estimote.indoorsdk_module.cloud.Location;

import java.util.HashMap;

/**
 * Created by masrofbayhaqqi on 1/2/18.
 */

public class CoreApplication extends Application {
    private static CoreApplication app;

    public static HashMap<String, Location> locationsById = new HashMap<String, Location>();
    public static EstimoteCloudCredentials cloudCredentials = new EstimoteCloudCredentials("tabsirul-anam-visionet-co--69i", "ccecb54050c95a655509c2005cbeb2d2");


    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
    }
}
