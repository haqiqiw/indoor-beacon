package id.co.visionet.indoorbeacon.activity;

import android.app.Notification;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.estimote.indoorsdk.IndoorLocationManagerBuilder;
import com.estimote.indoorsdk_module.algorithm.OnPositionUpdateListener;
import com.estimote.indoorsdk_module.algorithm.ScanningIndoorLocationManager;
import com.estimote.indoorsdk_module.cloud.Location;
import com.estimote.indoorsdk_module.cloud.LocationBeacon;
import com.estimote.indoorsdk_module.cloud.LocationLinearObject;
import com.estimote.indoorsdk_module.cloud.LocationPosition;
import com.estimote.indoorsdk_module.cloud.LocationWall;
import com.estimote.indoorsdk_module.view.IndoorLocationView;

import java.util.ArrayList;

import id.co.visionet.indoorbeacon.R;

import static id.co.visionet.indoorbeacon.core.CoreApplication.cloudCredentials;
import static id.co.visionet.indoorbeacon.core.CoreApplication.locationsById;

public class IndoorLocationActivity extends AppCompatActivity {

    private IndoorLocationView indoorLocationView;
    private ScanningIndoorLocationManager indoorLocationManager;
    private Location location;
    private Notification notification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_indoor_location);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        notification = new Notification.Builder(this)
                .setSmallIcon(R.drawable.beacon_gray)
                .setContentTitle("Estimote Inc. \u00AE")
                .setContentText("Indoor location is running...")
                .setPriority(Notification.PRIORITY_HIGH)
                .build();

        setupLocation();
        initView();
    }

    private void setupLocation() {
        Intent intent = getIntent();
        if (intent.getStringExtra("locationId") != null) {
            String locationId = intent.getStringExtra("locationId");
            if (locationsById.isEmpty()) {
                location = buildEmptyLocation();
            } else {
                location = locationsById.get(locationId);
            }
            getSupportActionBar().setTitle(location.getName());
        }
    }

    private Location buildEmptyLocation() {
        return new Location("", "", true, "", 0.0, new ArrayList<LocationWall>(), new ArrayList<LocationBeacon>(), new ArrayList<LocationLinearObject>());
    }

    private void initView() {
        indoorLocationView = findViewById(R.id.indoor_view);
        indoorLocationView.setLocation(location);

        indoorLocationManager =
                new IndoorLocationManagerBuilder(this, location, cloudCredentials)
                        .withScannerInForegroundService(notification)
                        .build();

        indoorLocationManager.setOnPositionUpdateListener(new OnPositionUpdateListener() {
            @Override
            public void onPositionUpdate(LocationPosition locationPosition) {
                indoorLocationView.updatePosition(locationPosition);
            }

            @Override
            public void onPositionOutsideLocation() {
                indoorLocationView.hidePosition();
            }
        });

        indoorLocationManager.startPositioning();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        indoorLocationManager.stopPositioning();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
