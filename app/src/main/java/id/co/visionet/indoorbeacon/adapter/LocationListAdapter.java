package id.co.visionet.indoorbeacon.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.estimote.indoorsdk_module.cloud.Location;

import java.util.ArrayList;
import java.util.List;

import id.co.visionet.indoorbeacon.R;

/**
 * Created by masrofbayhaqqi on 1/2/18.
 */

public class LocationListAdapter extends RecyclerView.Adapter<LocationListAdapter.ViewHolder> {

    private Context context;
    private List<Location> locations = new ArrayList<>();
    private onItemClickListener itemClickListener;

    public LocationListAdapter(Context context, List<Location> locations) {
        this.context = context;
        this.locations = locations;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_location_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LocationListAdapter.ViewHolder holder, int position) {
        Location location = locations.get(position);
        holder.name.setText(location.getName());
        holder.id.setText(location.getIdentifier());
    }

    @Override
    public int getItemCount() {
        return locations.size();
    }

    public interface onItemClickListener {
        public void onItemClick(View v, int position);
    }

    public void setOnItemClickListener(final onItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CardView card;
        TextView name;
        TextView id;

        public ViewHolder(View itemView) {
            super(itemView);
            card = itemView.findViewById(R.id.location_card);
            name = itemView.findViewById(R.id.location_name);
            id = itemView.findViewById(R.id.location_id);
            card.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onItemClick(view, getAdapterPosition());
        }
    }
}
